package com.example.myawesomeproject;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public  class CalculatorTest {

    @Test
    public void add() {
        Calculator calculator = new Calculator();
        assertEquals(2, calculator.add(1, 1));
        assertEquals(0, calculator.add(1, -1));
        assertEquals(0, calculator.add(-1, 1));
        assertEquals(-2, calculator.add(-1, -1));
    }

    @Test
    public void subtract() {
        Calculator calculator = new Calculator();
        assertEquals(0, calculator.subtract(1, 1));
        assertEquals(2, calculator.subtract(1, -1));
        assertEquals(-2, calculator.subtract(-1, 1));
        assertEquals(0, calculator.subtract(-1, -1));

    }

    @Test
    public void multiply() {
        Calculator calculator = new Calculator();
        assertEquals(1, calculator.multiply(1, 1));
        assertEquals(-1, calculator.multiply(1, -1));
        assertEquals(-1, calculator.multiply(-1, 1));
        assertEquals(1, calculator.multiply(-1, -1));
    }
}